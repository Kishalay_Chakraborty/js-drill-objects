const obj = {flavor: "chocolate"};
const defaultProps = {flavor: "vanilla", sprinkles: "lots"};
const defaults = require('../defaults.js');

const result = defaults(obj, defaultProps);

console.log(result);