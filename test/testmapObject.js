const obj = require('./data');
const mapObject = require('../mapObject');

const result = mapObject(obj, value => {

    return typeof value === 'number' ? value * 2 : value;
});

console.log(result);
