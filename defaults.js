function defaults(obj, defaultProps) {

    for(let object in defaultProps) {

        if(obj[object] === undefined) {
            obj[object] = defaultProps[object];
        }
    }

    return obj;
}

module.exports = defaults;