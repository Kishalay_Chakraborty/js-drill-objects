function pairs(obj) {

    let keyValuePair = [];

    for(let object in obj) {

        keyValuePair.push([object, obj[object]]);
    }

    return keyValuePair;
}

module.exports = pairs;