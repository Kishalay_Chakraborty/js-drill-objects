function invert(obj) {

    let duplicateObj = {};

    for(let object in obj) {

        duplicateObj[obj[object]] = object;
    }

    return duplicateObj;
}

module.exports = invert;