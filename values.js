function values(obj) {

    let values = [];

    for(let object in obj) {
        values.push(obj[object]);
    }

    return values;
}

module.exports = values;