function mapObject(obj, cb) {

    let result = {};

    for(let object in obj) {

        result[object] = cb(obj[object]);
    }

    return result;
}

module.exports = mapObject;