function keys(obj) {

    let keys = [];

    for(let object in obj) {
        keys.push(object);
    }

    return keys;
}

module.exports = keys;